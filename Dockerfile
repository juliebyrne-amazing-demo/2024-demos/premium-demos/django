FROM python:3.9-slim

# Install build deps, then run `pip install`, then remove unneeded build deps all in a single step.
# Correct the path to your production requirements file, if needed.
RUN set -ex \
    && BUILD_DEPS=" \
    build-essential \
    libpcre3-dev \
    libpq-dev \
    " \
    && apt-get update && apt-get install -y --no-install-recommends $BUILD_DEPS \
    && pip install --no-cache-dir uwsgi \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false $BUILD_DEPS \
    && rm -rf /var/lib/apt/lists/*

ENV PROJECT=autodevops
ENV CONTAINER_HOME=/srv
ENV CONTAINER_PROJECT=$CONTAINER_HOME/$PROJECT
ENV DJANGO_SETTINGS_MODULE=www.settings
ENV PYTHONPATH=.:${PYTHONPATH}

WORKDIR $CONTAINER_PROJECT

RUN mkdir -p $CONTAINER_PROJECT

COPY requirements.txt $CONTAINER_PROJECT
RUN pip install --no-cache-dir -r $CONTAINER_PROJECT/requirements.txt

COPY uwsgi.ini $CONTAINER_PROJECT
COPY www $CONTAINER_PROJECT/www
COPY $PROJECT $CONTAINER_PROJECT/$PROJECT

RUN django-admin collectstatic --noinput

#CMD django-admin migrate serve zero && django-admin migrate && django-admin load mcgis && uwsgi --ini uwsgi.ini
CMD django-admin migrate && uwsgi --ini uwsgi.ini

EXPOSE 5000
